package com.ruoyi.business.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.domain.TestTree;
import com.ruoyi.business.service.ITestTreeService;
import com.ruoyi.common.utils.poi.ExcelUtil;

import java.util.Arrays;
import java.util.List;

/**
 * test_treeController
 *
 * @author liutietou
 * @since 1.0 2025-01-09
 */
@RestController
@RequestMapping("/business/tree")
public class TestTreeController extends BaseController {
    @Autowired
    private ITestTreeService testTreeService;

    /**
     * 查询test_tree列表
     */
    @PreAuthorize("@ss.hasPermi('business:tree:list')")
    @GetMapping("/list")
    public AjaxResult list(TestTree testTree) {
        List<TestTree> list = testTreeService.findTestTreeList(testTree);
        return AjaxResult.success(list);
    }

    /**
     * 导出test_tree列表
     */
    @PreAuthorize("@ss.hasPermi('business:tree:export')")
    @Log(title = "test_tree", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TestTree testTree) {
        List<TestTree> list = testTreeService.findTestTreeList(testTree);
        ExcelUtil<TestTree> util = new ExcelUtil<>(TestTree.class);
        return util.exportExcel(list, "tree");
    }

    /**
     * 获取test_tree详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:tree:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(testTreeService.findById(id));
    }

    /**
     * 新增test_tree
     */
    @PreAuthorize("@ss.hasPermi('business:tree:add')")
    @Log(title = "test_tree", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TestTree testTree) {
        testTreeService.save(testTree);
        return AjaxResult.success();
    }

    /**
     * 修改test_tree
     */
    @PreAuthorize("@ss.hasPermi('business:tree:edit')")
    @Log(title = "test_tree", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TestTree testTree) {
        testTreeService.save(testTree);
        return AjaxResult.success();
    }

    /**
     * 删除test_tree
     */
    @PreAuthorize("@ss.hasPermi('business:tree:remove')")
    @Log(title = "test_tree", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        testTreeService.deleteByIds(Arrays.asList(ids));
        return AjaxResult.success();
    }
}
