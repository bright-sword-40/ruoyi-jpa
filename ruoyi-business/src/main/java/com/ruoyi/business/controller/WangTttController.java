package com.ruoyi.business.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.domain.WangTtt;
import com.ruoyi.business.service.IWangTttService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import java.util.Arrays;
import java.util.List;

/**
 * 测试代码生成器Controller
 *
 * @author liutietou
 * @since 1.0 2021-01-11
 */
@RestController
@RequestMapping("/business/ttt")
public class WangTttController extends BaseController {
    @Autowired
    private IWangTttService wangTttService;

    /**
     * 查询测试代码生成器列表
     */
    @PreAuthorize("@ss.hasPermi('business:ttt:list')")
    @GetMapping("/list")
    public TableDataInfo list(WangTtt wangTtt) {
        Page<WangTtt> page = wangTttService.findWangTttPage(wangTtt);
        return getDataTable(page);
    }

    /**
     * 导出测试代码生成器列表
     */
    @PreAuthorize("@ss.hasPermi('business:ttt:export')")
    @Log(title = "测试代码生成器", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WangTtt wangTtt) {
        List<WangTtt> list = wangTttService.findWangTttList(wangTtt);
        ExcelUtil<WangTtt> util = new ExcelUtil<>(WangTtt.class);
        return util.exportExcel(list, "ttt");
    }

    /**
     * 获取测试代码生成器详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:ttt:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(wangTttService.findById(id));
    }

    /**
     * 新增测试代码生成器
     */
    @PreAuthorize("@ss.hasPermi('business:ttt:add')")
    @Log(title = "测试代码生成器", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WangTtt wangTtt) {
        wangTttService.save(wangTtt);
        return AjaxResult.success();
    }

    /**
     * 修改测试代码生成器
     */
    @PreAuthorize("@ss.hasPermi('business:ttt:edit')")
    @Log(title = "测试代码生成器", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WangTtt wangTtt) {
        wangTttService.save(wangTtt);
        return AjaxResult.success();
    }

    /**
     * 删除测试代码生成器
     */
    @PreAuthorize("@ss.hasPermi('business:ttt:remove')")
    @Log(title = "测试代码生成器", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        wangTttService.deleteByIds(Arrays.asList(ids));
        return AjaxResult.success();
    }
}
