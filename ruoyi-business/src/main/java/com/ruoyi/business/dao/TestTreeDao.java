package com.ruoyi.business.dao;

import com.ruoyi.business.domain.TestTree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/**
 * test_treeDao接口
 *
 * @author liutietou
 * @since 1.0 2025-01-09
 */
@Repository
public interface TestTreeDao extends JpaRepository<TestTree, Long>, JpaSpecificationExecutor<TestTree> {



}
