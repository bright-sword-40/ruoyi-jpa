package com.ruoyi.business.dao;

import com.ruoyi.business.domain.WangTtt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/**
 * 测试代码生成器Dao接口
 *
 * @author liutietou
 * @since 1.0 2021-01-11
 */
@Repository
public interface WangTttDao extends JpaRepository<WangTtt, String>, JpaSpecificationExecutor<WangTtt> {



}
