package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;
import lombok.Data;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 * test_tree对象 test_tree
 *
 * @author liutietou
 * @since 1.0 2025-01-09
 */
@Entity
@Table(name = "test_tree")
@Data
public class TestTree extends TreeEntity {

    private static final long serialVersionUID = 1L;

    /**  */
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**  */
    @Excel(name = "")
    @Column(name="pid")
    private Long pid;

    /**  */
    @Excel(name = "")
    @Column(name="name")
    private String name;


}
