package com.ruoyi.business.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 * 测试代码生成器对象 wang_ttt
 *
 * @author liutietou
 * @since 1.0 2021-01-11
 */
@Entity
@Table(name = "wang_ttt")
@Data
public class WangTtt extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Id
    @GenericGenerator(name = "idGen", strategy = "uuid")
    @GeneratedValue(generator = "idGen")
    @Column(name="id")
    private String id;

    /** 字符串字段 */
    @Excel(name = "字符串字段")
    @Column(name="column_a")
    private String columnA;

    /** int字段 */
    @Excel(name = "int字段")
    @Column(name="column_b")
    private Long columnB;

    /** decimal字段 */
    @Excel(name = "decimal字段")
    @Column(name="column_c")
    private BigDecimal columnC;

    /** 创建时间 */
    @Column(name="create_time")
    private Date createTime;

    /** 创建者 */
    @Column(name="create_by")
    private String createBy;

    /** 更新时间 */
    @Column(name="update_time")
    private Date updateTime;

    /** 更新者 */
    @Column(name="update_by")
    private String updateBy;


}
