package com.ruoyi.business.service;

import java.util.List;
import org.springframework.data.domain.Page;
import com.ruoyi.business.domain.TestTree;

/**
 * test_treeService接口
 *
 * @author liutietou
 * @since 1.0 2025-01-09
 */
public interface ITestTreeService  {
    /**
     * 查询test_tree
     *
     * @param id test_treeID
     * @return test_tree
     */
    TestTree findById(Long id);

    /**
     * 分页查询test_tree列表
     *
     * @param req test_tree
     * @return test_tree集合
     */
    Page<TestTree> findTestTreePage(TestTree req);

    /**
     * 查询test_tree列表
     *
     * @param req test_tree
     * @return test_tree集合
     */
    List<TestTree> findTestTreeList(TestTree req);

    /**
     * 新增test_tree
     *
     * @param testTree test_tree
     * @return 结果
     */
    void save(TestTree testTree);

    /**
     * 批量删除test_tree
     *
     * @param ids 需要删除的test_treeID
     * @return 结果
     */
    void deleteByIds(List<Long> ids);

    /**
     * 删除test_tree信息
     *
     * @param id test_treeID
     * @return 结果
     */
    void deleteTestTreeById(Long id);
}
