package com.ruoyi.business.service;

import java.util.List;
import org.springframework.data.domain.Page;
import com.ruoyi.business.domain.WangTtt;

/**
 * 测试代码生成器Service接口
 *
 * @author liutietou
 * @since 1.0 2021-01-11
 */
public interface IWangTttService  {
    /**
     * 查询测试代码生成器
     *
     * @param id 测试代码生成器ID
     * @return 测试代码生成器
     */
    WangTtt findById(String id);

    /**
     * 分页查询测试代码生成器列表
     *
     * @param req 测试代码生成器
     * @return 测试代码生成器集合
     */
    Page<WangTtt> findWangTttPage(WangTtt req);

    /**
     * 查询测试代码生成器列表
     *
     * @param req 测试代码生成器
     * @return 测试代码生成器集合
     */
    List<WangTtt> findWangTttList(WangTtt req);

    /**
     * 新增测试代码生成器
     *
     * @param wangTtt 测试代码生成器
     * @return 结果
     */
    void save(WangTtt wangTtt);

    /**
     * 批量删除测试代码生成器
     *
     * @param ids 需要删除的测试代码生成器ID
     * @return 结果
     */
    void deleteByIds(List<String> ids);

    /**
     * 删除测试代码生成器信息
     *
     * @param id 测试代码生成器ID
     * @return 结果
     */
    void deleteWangTttById(String id);
}
