package com.ruoyi.common.core.dto;

import lombok.Data;

import java.math.BigInteger;
import java.util.Date;


/**
 * 菜单权限表 sys_menu
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Data
public class SysMenuDto {

    /**
     * 菜单ID
     */
    private BigInteger menuId;

    /**
     * 菜单名称
     */
    private String menuName;


    /**
     * 父菜单ID
     */
    private BigInteger parentId;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 是否为外链（0是 1否）
     */
    private Integer isFrame;

    /**
     * 是否缓存（0缓存 1不缓存）
     */
    private Integer isCache;

    /**
     * 类型（M目录 C菜单 F按钮）
     */
    private Character menuType;

    /**
     * 显示状态（0显示 1隐藏）
     */
    private Character visible;

    /**
     * 菜单状态（0显示 1隐藏）
     */
    private Character status;

    /**
     * 权限字符串
     */
    private String perms;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

}
