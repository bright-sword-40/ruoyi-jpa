package com.ruoyi.common.utils.code;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用枚举编码
 *
 * @author wintersnow
 * @since 1.0,  2020-12-15
 */
public class CommonBizCode {

    /**
     * 删除标记: 1.删除 0.正常
     */
    public enum DelFlag {

        YES("1", "删除"),

        NO("0", "正常");

        @Getter
        private String code;

        @Getter
        private String value;

        DelFlag(String code, String value) {
            this.code = code;
            this.value = value;
        }

        private static final Map<String, DelFlag> ENUM_MAP = new HashMap<>();

        static {
            DelFlag[] enumArr = DelFlag.values();
            for (DelFlag enumInfo : enumArr) {
                ENUM_MAP.put(enumInfo.getCode(), enumInfo);
            }
        }

        public static DelFlag asEnum(int code) {
            return ENUM_MAP.get(code);
        }
    }

}
