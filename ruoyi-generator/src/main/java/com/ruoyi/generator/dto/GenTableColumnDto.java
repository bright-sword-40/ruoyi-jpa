package com.ruoyi.generator.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;


/**
 * 代码生成业务字段表 gen_table_column
 *
 * @author liutietou
 * @since 1.0  2020-12-25
 */
@Data
public class GenTableColumnDto extends BaseEntity {


    /**
     * 列名称
     */
    private String columnName;


    /**
     * 是否必填（1是）
     */
    private String isRequired;


    /**
     * 是否主键（1是）
     */
    private String isPk;

    /**
     * 排序
     */
    private Integer sort;


    /**
     * 列描述
     */
    private String columnComment;


    /**
     * 是否自增（1是）
     */
    private String isIncrement;


    /**
     * 列类型
     */
    private String columnType;


}
