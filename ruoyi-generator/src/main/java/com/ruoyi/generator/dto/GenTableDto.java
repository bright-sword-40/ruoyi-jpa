package com.ruoyi.generator.dto;

import lombok.Data;

import java.util.Date;

/**
 * GenTableDto
 *
 * @author liutietou
 * @since 1.0,  2020-12-26
 */
@Data
public class GenTableDto {

    private String tableName;

    private String tableComment;

    private Date createTime;

    private Date updateTime;

}
