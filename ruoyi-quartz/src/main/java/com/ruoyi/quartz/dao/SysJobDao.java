package com.ruoyi.quartz.dao;

import com.ruoyi.quartz.domain.SysJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/**
 * 定时任务调度表 sys_job
 *
 * @author wintersnow
 * @since 1.0  2020-12-14
 */
@Repository
public interface SysJobDao extends JpaRepository<SysJob, Long>, JpaSpecificationExecutor<SysJob> {


}
