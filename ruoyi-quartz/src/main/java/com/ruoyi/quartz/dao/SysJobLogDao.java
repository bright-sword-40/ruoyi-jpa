package com.ruoyi.quartz.dao;

import com.ruoyi.quartz.domain.SysJobLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 定时任务调度日志表 sys_job_log
 *
 * @author wintersnow
 * @since 1.0  2020-12-14
 */
@Repository
public interface SysJobLogDao extends JpaRepository<SysJobLog, Long>, JpaSpecificationExecutor<SysJobLog> {

    Integer deleteByJobLogIdIn(List<Long> ids);

    @Modifying
    @Query(value = "truncate table sys_job_log", nativeQuery = true)
    void cleanJobLog();

}
