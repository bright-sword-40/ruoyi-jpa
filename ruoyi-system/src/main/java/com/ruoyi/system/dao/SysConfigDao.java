package com.ruoyi.system.dao;

import com.ruoyi.system.domain.SysConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 参数配置 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysConfigDao extends JpaRepository<SysConfig, Long>, JpaSpecificationExecutor<SysConfig> {

    List<SysConfig> findByConfigKey(String configKey);

}
