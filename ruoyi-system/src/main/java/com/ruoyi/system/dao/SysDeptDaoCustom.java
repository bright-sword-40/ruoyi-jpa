package com.ruoyi.system.dao;

import com.ruoyi.common.core.domain.entity.SysDept;

import java.util.List;

/**
 * 部门管理 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
public interface SysDeptDaoCustom {

    /**
     * @param roleId
     * @param deptCheckStrictly 部门树选择项是否关联显示（0：父子不互相关联显示 1：父子互相关联显示 ）
     * @return
     */
    List<Long> findDeptListByRoleId(Long roleId, boolean deptCheckStrictly);

    List<SysDept> findDeptList(SysDept req);

}
