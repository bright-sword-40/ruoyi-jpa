package com.ruoyi.system.dao;

import com.ruoyi.system.domain.SysLogininfor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysLogininforDao extends JpaRepository<SysLogininfor, Long>, JpaSpecificationExecutor<SysLogininfor> {

    void deleteAllByInfoIdIn(List<Long> ids);

    @Modifying
    @Query(value = "truncate table sys_logininfor", nativeQuery = true)
    void truncateTable();

}
