package com.ruoyi.system.dao;


import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.core.domain.entity.SysRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * 菜单表 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
public interface SysMenuDaoCustom {

    List<SysMenu> findMenuListByUserId(SysMenu menu);

    List<String> findMenuPermsByUserId(Long userId);

    List<Long> findMenuListByRoleId(Long roleId, boolean menuCheckStrictly);

    Page<SysRole> findMixPage(SysRole req, Pageable pageable);

    List<SysRole> findRolePermissionByUserId(Long userId);

    List<SysMenu> findMenuTreeAll();

    List<SysMenu> findMenuTreeByUserId(Long userId);
}
