package com.ruoyi.system.dao;

import com.ruoyi.system.domain.SysRoleMenu;
import com.ruoyi.system.domain.groupkey.SysRoleMenuKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色与菜单关联表 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysRoleMenuDao extends JpaRepository<SysRoleMenu, SysRoleMenuKey>, JpaSpecificationExecutor<SysRoleMenu>, SysMenuDaoCustom {

    @Query(value = " select count(1) from sys_role_menu where menu_id = ?1   ", nativeQuery = true)
    Integer checkMenuExistRole(Long menuId);

    void deleteByRoleId(Long roleId);

    void deleteByRoleIdIn(List<Long> roleId);

    List<SysRoleMenu> findByRoleId(Long roleId);

}
