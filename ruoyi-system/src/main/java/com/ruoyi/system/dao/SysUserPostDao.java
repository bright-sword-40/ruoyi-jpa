package com.ruoyi.system.dao;

import com.ruoyi.system.domain.SysUserPost;
import com.ruoyi.system.domain.groupkey.SysUserPostKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户与岗位关联表 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysUserPostDao extends JpaRepository<SysUserPost, SysUserPostKey>, JpaSpecificationExecutor<SysUserPost>, SysMenuDaoCustom {

    @Query(value = "select count(1) from sys_user_post where post_id=?1 ", nativeQuery = true)
    Integer countUserPostById(Long postId);

    void deleteByUserId(Long userId);

    Integer deleteByUserIdIn(List<Long> userId);

    List<SysUserPost> findByUserId(Long userId);

}
