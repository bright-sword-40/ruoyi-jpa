package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.ColumnType;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;


/**
 * 系统访问记录表 sys_logininfor
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Entity
@Table(name = "sys_logininfor")
@Data
public class SysLogininfor extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @Excel(name = "序号", cellType = ColumnType.NUMERIC)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "info_id")
    private Long infoId;

    /**
     * 用户账号
     */
    @Excel(name = "用户账号")
    @Column(name = "user_name")
    private String userName;

    /**
     * 登录状态 0成功 1失败
     */
    @Excel(name = "登录状态", readConverterExp = "0=成功,1=失败")
    @Column(name = "status")
    private String status;

    /**
     * 登录IP地址
     */
    @Excel(name = "登录地址")
    @Column(name = "ipaddr")
    private String ipaddr;

    /**
     * 登录地点
     */
    @Excel(name = "登录地点")
    @Column(name = "login_location")
    private String loginLocation;

    /**
     * 浏览器类型
     */
    @Excel(name = "浏览器")
    @Column(name = "browser")
    private String browser;

    /**
     * 操作系统
     */
    @Excel(name = "操作系统")
    @Column(name = "os")
    private String os;

    /**
     * 提示消息
     */
    @Excel(name = "提示消息")
    @Column(name = "msg")
    private String msg;

    /**
     * 访问时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "访问时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "login_time")
    private Date loginTime;


}
