package com.ruoyi.system.domain;

import com.ruoyi.system.domain.groupkey.SysRoleDeptKey;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 角色和部门关联 sys_role_dept
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Entity
@Table(name = "sys_role_dept")
@IdClass(SysRoleDeptKey.class)
@Data
public class SysRoleDept implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    @Id
    @Column(name = "role_id")
    private Long roleId;

    /**
     * 部门ID
     */
    @Id
    @Column(name = "dept_id")
    private Long deptId;

}
