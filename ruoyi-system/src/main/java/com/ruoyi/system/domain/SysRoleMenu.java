package com.ruoyi.system.domain;

import com.ruoyi.system.domain.groupkey.SysRoleMenuKey;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 角色和菜单关联 sys_role_menu
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Entity
@Table(name = "sys_role_menu")
@IdClass(SysRoleMenuKey.class)
@Data
public class SysRoleMenu implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    @Id
    @Column(name = "role_id")
    private Long roleId;

    /**
     * 菜单ID
     */
    @Id
    @Column(name = "menu_id")
    private Long menuId;

}
