package com.ruoyi.system.domain;

import com.ruoyi.system.domain.groupkey.SysUserPostKey;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 用户和岗位关联 sys_user_post
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Entity
@Table(name = "sys_user_post")
@IdClass(SysUserPostKey.class)
@Data
public class SysUserPost implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    @Id
    @Column(name = "user_id")
    private Long userId;

    /**
     * 岗位ID
     */
    @Id
    @Column(name = "post_id")
    private Long postId;

}
