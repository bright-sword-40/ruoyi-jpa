package com.ruoyi.system.domain.groupkey;

import lombok.Data;

import java.io.Serializable;

/**
 * 角色部门表 联合主键
 *
 * @author liutietou
 * @since 1.0,  2020-12-16
 */
@Data
public class SysRoleDeptKey implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 部门id
     */
    private Long deptId;

}
