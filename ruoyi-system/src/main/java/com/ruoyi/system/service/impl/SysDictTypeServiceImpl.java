package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysDictType;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DictUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.code.BusinessBizCode;
import com.ruoyi.common.utils.sql.SqlUtil;
import com.ruoyi.system.dao.SysDictDataDao;
import com.ruoyi.system.dao.SysDictTypeDao;
import com.ruoyi.system.service.ISysDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;


/**
 * 字典 业务层处理
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Transactional(readOnly = true)
@Service
public class SysDictTypeServiceImpl implements ISysDictTypeService {

    @Autowired
    private SysDictTypeDao dictTypeDao;

    @Autowired
    private SysDictDataDao dictDataDao;

    /**
     * 项目启动时，初始化字典到缓存
     */
    @PostConstruct
    public void init() {
        List<SysDictType> dictTypeList = dictTypeDao.findAll();
        for (SysDictType dictType : dictTypeList) {
            List<SysDictData> dictDatas = dictDataDao.findByDictTypeAndStatusOrderByDictSortAsc(dictType.getDictType(), "0");
            DictUtils.setDictCache(dictType.getDictType(), dictDatas);
        }
    }

    /**
     * 根据条件分页查询字典类型
     *
     * @param req 字典类型信息
     * @return 字典类型集合信息
     */
    @Override
    public Page<SysDictType> selectDictTypeList(SysDictType req) {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        if (StringUtils.isNotNull(pageDomain.getPageNum()) && StringUtils.isNotNull(pageDomain.getPageSize())) {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
        }
        Specification<SysDictType> example = new Specification<SysDictType>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<SysDictType> root,
                                         CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> list = new ArrayList<>();
                if (StringUtils.isNoneBlank(req.getDictName())) {
                    Predicate pre = cb.like(root.get("dictName").as(String.class), "%" + req.getDictName() + "%");
                    list.add(pre);
                }
                if (StringUtils.isNoneBlank(req.getDictType())) {
                    Predicate pre = cb.like(root.get("dictType").as(String.class), "%" + req.getDictType() + "%");
                    list.add(pre);
                }
                if (StringUtils.isNoneBlank(req.getStatus())) {
                    Predicate pre = cb.equal(root.get("status").as(String.class), req.getStatus());
                    list.add(pre);
                }
                if (null != req.getParams().get("beginTime")) {
                    Predicate pre = cb.greaterThanOrEqualTo(root.get("createTime").as(Date.class), (Date) req.getParams().get("beginTime"));
                    list.add(pre);
                }
                if (null != req.getParams().get("endTime")) {
                    Predicate pre = cb.lessThanOrEqualTo(root.get("createTime").as(Date.class), (Date) req.getParams().get("endTime"));
                    list.add(pre);
                }
                if (list.isEmpty()) {
                    return null;
                }
                return cb.and(list.toArray(new Predicate[0]));
            }
        };
        Pageable pageable = PageRequest.of(pageDomain.getPageNo(),
                Optional.ofNullable(pageDomain.getPageSize()).orElse(PageDomain.DEFAULT_PAGE_SIZE),
                Sort.Direction.DESC,
                Optional.ofNullable(pageDomain.getOrderByColumn()).orElse("createTime"));
        Page<SysDictType> page = dictTypeDao.findAll(example, pageable);
        return page;
    }

    /**
     * 根据所有字典类型
     *
     * @return 字典类型集合信息
     */
    @Override
    public List<SysDictType> selectDictTypeAll() {
        return dictTypeDao.findAll();
    }

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataByType(String dictType) {
        List<SysDictData> dictDatas = DictUtils.getDictCache(dictType);
        if (StringUtils.isNotEmpty(dictDatas)) {
            return dictDatas;
        }
        dictDatas = dictDataDao.findByDictTypeAndStatusOrderByDictSortAsc(dictType, "0");
        if (StringUtils.isNotEmpty(dictDatas)) {
            DictUtils.setDictCache(dictType, dictDatas);
            return dictDatas;
        }
        return null;
    }

    /**
     * 根据字典类型ID查询信息
     *
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    @Override
    public SysDictType selectDictTypeById(Long dictId) {
        return dictTypeDao.findById(dictId).orElse(new SysDictType());
    }

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    @Override
    public SysDictType selectDictTypeByType(String dictType) {
        SysDictType sysDictType = dictTypeDao.findByDictType(dictType).get(0);
        return sysDictType;
    }

    /**
     * 批量删除字典类型信息
     *
     * @param dictIds 需要删除的字典ID
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteDictTypeByIds(Long[] dictIds) {
        for (Long dictId : dictIds) {
            SysDictType dictType = selectDictTypeById(dictId);
            int size = dictDataDao.findByDictType(dictType.getDictType()).size();
            if (size > 0) {
                throw new CustomException(String.format("%1$s已分配,不能删除", dictType.getDictName()));
            }
        }
        List<SysDictType> allById = dictTypeDao.findAllById(Arrays.asList(dictIds));
        dictTypeDao.deleteInBatch(allById);
        DictUtils.clearDictCache();
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 清空缓存数据
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void clearCache() {
        DictUtils.clearDictCache();
    }

    /**
     * 新增保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertDictType(SysDictType dictType) {
        dictType.setCreateTime(new Date());
        SysDictType save = dictTypeDao.save(dictType);
        if (null != save) {
            DictUtils.clearDictCache();
        }
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 修改保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateDictType(SysDictType dictType) {
        SysDictType oldDict = dictTypeDao.findById(dictType.getDictId()).orElse(new SysDictType());
        dictDataDao.updateType(oldDict.getDictType(), dictType.getDictType());
        SysDictType save = dictTypeDao.save(dictType);
        if (null != save) {
            DictUtils.clearDictCache();
        }
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 校验字典类型称是否唯一
     *
     * @param dict 字典类型
     * @return 结果
     */
    @Override
    public String checkDictTypeUnique(SysDictType dict) {
        Long dictId = StringUtils.isNull(dict.getDictId()) ? -1L : dict.getDictId();
        List<SysDictType> sysDictTypes = dictTypeDao.findByDictType(dict.getDictType());
        if (!sysDictTypes.isEmpty() && sysDictTypes.get(0).getDictId().longValue() != dictId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
}
