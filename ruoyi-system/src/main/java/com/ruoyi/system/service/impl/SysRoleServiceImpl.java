package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.code.BusinessBizCode;
import com.ruoyi.common.utils.code.CommonBizCode;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.dao.SysRoleDao;
import com.ruoyi.system.dao.SysRoleDeptDao;
import com.ruoyi.system.dao.SysRoleMenuDao;
import com.ruoyi.system.dao.SysUserRoleDao;
import com.ruoyi.system.domain.SysRoleDept;
import com.ruoyi.system.domain.SysRoleMenu;
import com.ruoyi.system.service.ISysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;


/**
 * 角色 业务层处理
 *
 * @author wintersnow
 * @since 1.0  2020-12-12
 */
@Slf4j
@Transactional(readOnly = true)
@Service
public class SysRoleServiceImpl implements ISysRoleService {

    @Qualifier("microSvcExecutor")
    @Autowired
    private ExecutorService executorService;

    @Autowired
    private SysRoleDao roleDao;

    @Autowired
    private SysRoleMenuDao roleMenuDao;

    @Autowired
    private SysUserRoleDao userRoleDao;

    @Autowired
    private SysRoleDeptDao roleDeptDao;

    @Autowired
    private EntityManager entityManager;


    /**
     * 根据条件分页查询角色数据
     *
     * @param req 角色信息
     * @return 角色数据集合信息
     */
    @Override
    @DataScope(deptAlias = "d")
    public Page<SysRole> findPage(SysRole req) {
        PageDomain pageReq = TableSupport.buildPageRequest();
        Page<SysRole> page = roleDao.findMixPage(req, PageRequest.of(pageReq.getPageNum() - 1, pageReq.getPageSize()));
        return page;

    }

    @Override
    public List<SysRole> selectRoleList(SysRole role) {
        return roleDao.findAll();
    }


    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectRolePermissionByUserId(Long userId) {
        List<SysRole> perms = roleDao.findRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRole perm : perms) {
            if (StringUtils.isNotNull(perm)) {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 查询所有角色
     *
     * @return 角色列表
     */
    @Override
    public List<SysRole> selectRoleAll() {
        return SpringUtils.getAopProxy(this).selectRoleList(new SysRole());
    }

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    @Override
    public List<Long> selectRoleListByUserId(Long userId) {
        return roleDao.findRoleListByUserId(userId);
    }

    /**
     * 通过角色ID查询角色
     *
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    @Override
    public SysRole selectRoleById(Long roleId) {
        return roleDao.findById(roleId).orElse(new SysRole());
    }

    /**
     * 校验角色名称是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleNameUnique(SysRole role) {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        List<SysRole> roles = roleDao.findByRoleName(role.getRoleName());

        if (!roles.isEmpty() && roles.get(0).getRoleId().longValue() != roleId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色权限是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleKeyUnique(SysRole role) {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        List<SysRole> roles = roleDao.findByRoleKey(role.getRoleKey());
        if (!roles.isEmpty() && roles.get(0).getRoleId().longValue() != roleId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色是否允许操作
     *
     * @param role 角色信息
     */
    @Override
    public void checkRoleAllowed(SysRole role) {
        if (StringUtils.isNotNull(role.getRoleId()) && role.isAdmin()) {
            throw new CustomException("不允许操作超级管理员角色");
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public int countUserRoleByRoleId(Long roleId) {
        return userRoleDao.countUserRoleByRoleId(roleId);
    }

    /**
     * 新增保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertRole(SysRole role) {
        // 新增角色信息
        role.setCreateTime(new Date());
        role.setDelFlag(CommonBizCode.DelFlag.NO.getCode());
        roleDao.save(role);
        return insertRoleMenu(role);
    }

    /**
     * 修改保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateRole(SysRole role) {
        // 修改角色信息
        roleDao.save(role);
        // 删除角色与菜单关联
        List<SysRoleMenu> byRoleId = roleMenuDao.findByRoleId(role.getRoleId());
        if (!byRoleId.isEmpty()) {
            roleMenuDao.deleteInBatch(byRoleId);
        }
        return insertRoleMenu(role);
    }

    /**
     * 修改角色状态
     *
     * @param role 角色信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateRoleStatus(SysRole role) {
        SysRole sysRole = roleDao.findById(role.getRoleId()).get();
        sysRole.setStatus(role.getStatus());
        sysRole.setUpdateBy(role.getUpdateBy());
        sysRole.setUpdateTime(new Date());
        roleDao.save(sysRole);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 修改数据权限信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int authDataScope(SysRole role) {
        // 修改角色信息
        roleDao.save(role);
        // 删除角色与部门关联
        List<SysRoleDept> existRoleDept = roleDeptDao.findByRoleId(role.getRoleId());
        if (!existRoleDept.isEmpty()) {
            roleDeptDao.deleteInBatch(existRoleDept);
        }
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(role);
    }

    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertRoleMenu(SysRole role) {
        // 新增用户与角色管理
        List<SysRoleMenu> list = new ArrayList<>();
        for (Long menuId : role.getMenuIds()) {
            SysRoleMenu rm = new SysRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (list.size() > 0) {
            roleMenuDao.saveAll(list);
            //TODO 批量保存缓慢sql 待处理
//            Function<Object, Object> saveFunction = bean -> roleMenuDao.save((SysRoleMenu) bean);
//            batchSave(list, saveFunction, executorService);
//            batchSave2(list);
//            batchInsertIk(list);
        }
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    @Autowired
    public JdbcTemplate jdbcTemplate;

    public Integer batchInsertIk(List<SysRoleMenu> ikList) {
        String sql = "insert into sys_role_dept(role_id, dept_id) values(?,?)";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Long roleId = ikList.get(i).getRoleId();
                Long menuId = ikList.get(i).getMenuId();
                ps.setLong(1, roleId);
                ps.setLong(2, menuId);
            }

            @Override
            public int getBatchSize() {
                return ikList.size();
            }
        });
        return 0;
    }

    private void batchSave(List<SysRoleMenu> rows, Function<Object, Object> rowFunction, ExecutorService excelExecutor) {
        Function<Object, CompletableFuture> rowFutureFunction = row -> CompletableFuture.supplyAsync(() -> rowFunction.apply(row), excelExecutor);
        CompletableFuture[] futures = rows.stream().map(rowFutureFunction::apply).toArray(size -> new CompletableFuture[size]);
        CompletableFuture.allOf(futures).join();
    }

    public void batchSave2(List<SysRoleMenu> list) {
        int j = 0;
        for (SysRoleMenu entity : list) {
            //TODO 判断数据的合法性
            entityManager.persist(entity);
            j++;
            if (j % 50 == 0 || j == list.size()) {
                try {
                    entityManager.flush();
                } catch (Exception e) {
                    log.error("fail", e);
                } finally {
                    entityManager.clear();
                }

            }

        }
    }


    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertRoleDept(SysRole role) {
        // 新增角色与部门（数据权限）管理
        List<SysRoleDept> list = new ArrayList<>();
        for (Long deptId : role.getDeptIds()) {
            SysRoleDept rd = new SysRoleDept();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0) {
            roleDeptDao.saveAll(list);
        }
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 通过角色ID删除角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRoleById(Long roleId) {
        // 删除角色与菜单关联
        List<SysRoleMenu> byRoleId = roleMenuDao.findByRoleId(roleId);
        if (!byRoleId.isEmpty()) {
            roleMenuDao.deleteInBatch(byRoleId);
        }
        // 删除角色与部门关联
        List<SysRoleDept> existsRoleDept = roleDeptDao.findByRoleId(roleId);
        if (!existsRoleDept.isEmpty()) {
            roleDeptDao.deleteInBatch(existsRoleDept);
        }
        if (roleDao.existsById(roleId)) {
            roleDao.deleteByRoleId(roleId);
        }
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 批量删除角色信息
     *
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRoleByIds(Long[] roleIds) {
        for (Long roleId : roleIds) {
            checkRoleAllowed(new SysRole(roleId));
            SysRole role = selectRoleById(roleId);
            if (countUserRoleByRoleId(roleId) > 0) {
                throw new CustomException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
        }
        // 删除角色与菜单关联
        roleMenuDao.deleteByRoleIdIn(Arrays.asList(roleIds));
        // 删除角色与部门关联
        roleDeptDao.deleteByRoleIdIn(Arrays.asList(roleIds));
        roleDao.deleteByRoleIdIn(Arrays.asList(roleIds));
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }
}
