import request from '@/utils/request'

// 查询test_tree列表
export function listTree(query) {
  return request({
    url: '/business/tree/list',
    method: 'get',
    params: query
  })
}

// 查询test_tree详细
export function getTree(id) {
  return request({
    url: '/business/tree/' + id,
    method: 'get'
  })
}

// 新增test_tree
export function addTree(data) {
  return request({
    url: '/business/tree',
    method: 'post',
    data: data
  })
}

// 修改test_tree
export function updateTree(data) {
  return request({
    url: '/business/tree',
    method: 'put',
    data: data
  })
}

// 删除test_tree
export function delTree(id) {
  return request({
    url: '/business/tree/' + id,
    method: 'delete'
  })
}

// 导出test_tree
export function exportTree(query) {
  return request({
    url: '/business/tree/export',
    method: 'get',
    params: query
  })
}